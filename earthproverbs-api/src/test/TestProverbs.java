package test;

import junit.framework.TestCase;

import com.proverby.api.EarthProverbsApi;
import com.proverby.api.EarthProverbsListener;
import com.proverby.api.ParseStatus;
import com.proverby.api.Proverb;
import com.proverby.api.Query;
import com.proverby.api.param.CountryParameter;
import com.proverby.api.param.CriteriaParameter;
import com.proverby.api.param.MaxResultsParameter;
import com.proverby.api.param.PageParameter;
import com.proverby.api.param.RegionParameter;
import com.proverby.api.param.SortOrder;
import com.proverby.api.param.SortParameter;
import com.proverby.api.param.TagParameter;

public class TestProverbs extends TestCase
{
	EarthProverbsApi api = EarthProverbsApi.getInstance();
	EarthProverbsListener l = new EarthProverbsListener() {
		@Override
		public void loading()
		{
			assert(true);
		}

		@Override
		public void finished(ParseStatus status )
		{
			assertEquals( ParseStatus.SUCCESS, status );
			assertTrue ( api.getProverbs().size() > 0 );
		}
	};

	EarthProverbsListener detailListener = new EarthProverbsListener() {
		@Override
		public void loading()
		{
			System.out.println("Loading...");
		}

		@Override
		public void finished( ParseStatus status )
		{
			int i = 0;
			for (Proverb proverb : api.getProverbs())
			{
				System.out.println(++i + ": " + proverb.getDescription());
			}
		}
	};

	public void testListProverbs()
	{
		api.attachProverbListener(l);
		api.listProverbs(Query.createProverbListQuery());
	}

	public void testFilterProverbs()
	{
		api.attachProverbListener(detailListener);
		Query query = Query.createSearchProverbQuery()
				.addParameter(CriteriaParameter.create("leader"));
		query.addParameter(SortParameter.create(SortOrder.NAME_DESCENDING));
		api.listProverbs(query);
	}

	public void testDailyProverb()
	{
		api.attachProverbListener(detailListener);
		api.listProverbs(Query.createDailyProverbQuery());
	}

	public void testListProverbsByCountry()
	{
		api.attachProverbListener(l);
		Query query = Query.createListProverbByCountryQuery();
		query.addParameter( CountryParameter.create("Trinidad and Tobago")) ;
		api.listProverbs(query);
	}
	
	public void testListProverbsByRegion()
	{
		api.attachProverbListener(l);
		Query query = Query.createListProverbByCountryQuery();
		query.addParameter( RegionParameter.create("africa")) ;
		api.listProverbs(query);
	}
	
	public void testListProverbsByTag()
	{
		api.attachProverbListener(l);
		Query query = Query.createListProverbByCountryQuery();
		query.addParameter( TagParameter.create("god")) ;
		api.listProverbs(query);
	}

	public void testSearchProverbs()
	{
		api.attachProverbListener(detailListener);

		Query query = Query.createSearchProverbQuery();
		query.addParameter(CriteriaParameter.create("leader"));
		query.addParameter(PageParameter.create(1));
		query.addParameter(new MaxResultsParameter(25));
		api.listProverbs(query);

		query = Query.createSearchProverbQuery();
		query.addParameter(CriteriaParameter.create("leader"));
		query.addParameter(PageParameter.create(2));
		query.addParameter(new MaxResultsParameter(25));
		api.listProverbs(query);

		query = Query.createSearchProverbQuery();
		query.addParameter(CriteriaParameter.create("leader"));
		query.addParameter(PageParameter.create(3));
		query.addParameter(new MaxResultsParameter(25));
		api.listProverbs(query);

		query = Query.createSearchProverbQuery();
		query.addParameter(CriteriaParameter.create("leader"));
		query.addParameter(PageParameter.create(4));
		query.addParameter(new MaxResultsParameter(25));
		api.listProverbs(query);
	}
}