package test;

import com.proverby.api.EarthProverbsApi;
import com.proverby.api.EarthProverbsListener;
import com.proverby.api.ParseStatus;
import com.proverby.api.Query;
import com.proverby.api.param.TimeParameter;

import junit.framework.TestCase;

public class TestCountries extends TestCase
{
	EarthProverbsApi api = EarthProverbsApi.getInstance();

	EarthProverbsListener c = new EarthProverbsListener() {
		@Override
		public void loading()
		{
			System.out.println("Loading Countries...");
		}

		@Override
		public void finished( ParseStatus status )
		{
			System.out.println(String.format("%s: Loaded %d countries",
					status, api.getCountries().size()));
		}
	};

	public void testListCountries()
	{
		api.attachCountryListener(c);
		api.listCountries(Query.createCountryListQuery().addParameter(
				TimeParameter.create(0)));
	}
}