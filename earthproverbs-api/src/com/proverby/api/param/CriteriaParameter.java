package com.proverby.api.param;

import java.util.ArrayList;
import java.util.List;

public class CriteriaParameter extends Parameter
{
	private int index;
	private String value;

	private CriteriaParameter(int index, String value)
	{
		this.index = index;
		this.value = value;
	}

	public static CriteriaParameter[] create( String criteria )
	{
		List<CriteriaParameter> list = new ArrayList<CriteriaParameter>();
		if (criteria != null && !criteria.isEmpty())
		{
			String[] matches = criteria.split(" ");
			for (String match : matches)
			{
				if (match != null && !match.isEmpty())
				{
					if (!match.trim().isEmpty())
					{
						int i = list.size();
						list.add( new CriteriaParameter(i, match));
					}
				}
			}
		}
		CriteriaParameter[] params = new CriteriaParameter[list.size()];
		return list.toArray( params );
	}
	
	@Override
	public String getName()
	{
		return String.format("%s[%d]", ParameterName.MATCHES.getName(), index);
	}

	@Override
	public String getValue()
	{
		return value;
	}
}