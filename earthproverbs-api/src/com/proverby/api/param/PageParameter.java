package com.proverby.api.param;

public class PageParameter extends Parameter
{
	private int page;

	private PageParameter(int pageNumber)
	{
		this.page = pageNumber;
	}

	public static PageParameter create( int page )
	{
		if ( page <= 0 )
			return null;
		return new PageParameter( page );
	}
	
	@Override
	public String getName()
	{
		return ParameterName.PAGE.getName();
	}

	@Override
	public String getValue()
	{
		return String.valueOf(page);
	}
}
