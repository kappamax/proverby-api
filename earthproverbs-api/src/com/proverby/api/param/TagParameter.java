package com.proverby.api.param;

public class TagParameter extends Parameter
{
	private String tag;

	private TagParameter(String tag)
	{
		this.tag = tag;
	}

	public static TagParameter create( String tag )
	{
		if ( tag == null )
			return null;
		return new TagParameter( tag );
	}
	
	@Override
	public String getName()
	{
		return ParameterName.TAG.getName();
	}

	@Override
	public String getValue()
	{
		return tag;
	}
}
