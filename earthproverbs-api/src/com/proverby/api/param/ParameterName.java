package com.proverby.api.param;

enum ParameterName
{
	MATCHES("matches"),
	SINCE("since"),
	PAGE("page"),
	ORDER("order"),
	MAXRESULTS("max"),
	TAG("tag"),
	REGION("region"),
	COUNTRY("country");
	
	private String name;
	
	private ParameterName( String name )
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
}