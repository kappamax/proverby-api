package com.proverby.api.param;

public class SortParameter extends Parameter
{
	private SortOrder order;

	private SortParameter(SortOrder order)
	{
		this.order = order;
	}
	
	public static SortParameter create( SortOrder order )
	{
		if ( order == null )
			return null;
		return new SortParameter(order);
	}

	@Override
	public String getName()
	{
		return ParameterName.ORDER.getName();
	}

	@Override
	public String getValue()
	{
		return String.valueOf(order.getOrder());
	}
}