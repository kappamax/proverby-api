package com.proverby.api.param;

public class RegionParameter extends Parameter
{
	private String region;

	private RegionParameter(String region)
	{
		this.region = region;
	}

	public static RegionParameter create( String region )
	{
		if ( region == null )
			return null;
		return new RegionParameter( region );
	}
	
	@Override
	public String getName()
	{
		return ParameterName.REGION.getName();
	}

	@Override
	public String getValue()
	{
		return region;
	}
}
