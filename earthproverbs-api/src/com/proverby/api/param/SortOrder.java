package com.proverby.api.param;

public enum SortOrder
{
	NAME_ASCENDING(0),
	DATE_ASCENDING(1),
	NAME_DESCENDING(2),
	DATE_DESCENDING(3);
	
	private int order;
	
	private SortOrder(int value)
	{
		this.order = value;
	}
	
	public int getOrder()
	{
		return order;
	}
}