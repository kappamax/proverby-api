package com.proverby.api.param;

public class CountryParameter extends Parameter
{
	private String country;

	private CountryParameter(String country)
	{
		this.country = country;
	}

	public static CountryParameter create( String country )
	{
		if ( country == null )
			return null;
		return new CountryParameter( country );
	}
	
	@Override
	public String getName()
	{
		return ParameterName.COUNTRY.getName();
	}

	@Override
	public String getValue()
	{
		return country;
	}
}
