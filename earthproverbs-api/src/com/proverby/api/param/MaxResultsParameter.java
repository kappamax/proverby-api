package com.proverby.api.param;

public class MaxResultsParameter extends Parameter
{
	private int max;

	public MaxResultsParameter(int max)
	{
		this.max = max;
	}

	public static MaxResultsParameter create( int max )
	{
		if ( max <= 0 )
			return null;
		return new MaxResultsParameter(max);
	}

	@Override
	public String getName()
	{
		return ParameterName.MAXRESULTS.getName();
	}

	@Override
	public String getValue()
	{
		return String.valueOf(max);
	}
}