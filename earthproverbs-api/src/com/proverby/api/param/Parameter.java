package com.proverby.api.param;

public abstract class Parameter
{
	public abstract String getName();
	
	public abstract String getValue();
	
	public String toString()
	{
		return String.format("&%s=%s", getName(), getValue() );
	}
}