package com.proverby.api.param;

public class TimeParameter extends Parameter
{
	private long time;

	private TimeParameter(long lastUpdatedMillis)
	{
		this.time = lastUpdatedMillis;
	}
	
	public static TimeParameter create( long lastUpdatedMillis )
	{
		if ( lastUpdatedMillis <= 0 )
			return null;
		return new TimeParameter(lastUpdatedMillis);
	}

	@Override
	public String getName()
	{
		return ParameterName.SINCE.getName();
	}

	@Override
	public String getValue()
	{
		return String.valueOf(time);
	}
}