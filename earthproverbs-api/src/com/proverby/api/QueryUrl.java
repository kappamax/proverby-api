package com.proverby.api;

enum QueryUrl
{
	SEARCH_PROVERBS("http://proverby.com/search.html?"),
	LIST_PROVERBS("http://proverby.com/listProverbs.html?"),
	LIST_TAG_PROVERBS("http://proverby.com/listTagProverbs.html?"),
	LIST_COUNTRY_PROVERBS("http://proverby.com/listCountryProverbs.html?"),
	LIST_REGION_PROVERBS("http://proverby.com/listRegionProverbs.html?"),
	LIST_COUNTRIES("http://proverby.com/listCountries.html?"),
	LIST_REGIONS("http://proverby.com/listRegions.html?"),
	LIST_TAGS("http://proverby.com/listTags.html?"),
	DAILY_PROVERB("http://proverby.com/dailyProverb.html?");

	private String url;
	
	private QueryUrl( String url )
	{
		this.url = url;
	}
	
	public String getUrl()
	{
		return url;
	}
}