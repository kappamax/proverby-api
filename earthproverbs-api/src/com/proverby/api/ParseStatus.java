package com.proverby.api;

public enum ParseStatus
{
	SUCCESS, JSON_ERROR, NETWORK_FAILURE, TRY_AGAIN, CANCELLED, LOADING;
}
