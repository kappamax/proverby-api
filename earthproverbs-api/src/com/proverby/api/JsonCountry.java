package com.proverby.api;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

class JsonCountry implements Country
{
	private String name;

	private String code;

	private String background;

	private long lastUpdated;

	private int sortOrder;

	private double[] location;

	private List<String> regions = new ArrayList<String>();

	public JsonCountry()
	{

	}

	@JsonProperty("sort_order")
	public void setSortOrder(int sortOrder)
	{
		this.sortOrder = sortOrder;
	}

	public void setRegions(List<String> regions)
	{
		this.regions = regions;
	}

	public void setBackground(String background)
	{
		this.background = background;
	}

	@JsonProperty("country_code")
	public void setCode(String code)
	{
		this.code = code;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setLastUpdated(long lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}

	public String getName()
	{
		return name;
	}

	@JsonProperty("country_code")
	public String getCode()
	{
		return code;
	}

	public long getLastUpdated()
	{
		return lastUpdated;
	}

	public double[] getLocation()
	{
		return location;
	}

	public String getBackground()
	{
		return background;
	}

	public List<String> getRegions()
	{
		return regions;
	}

	@JsonProperty("sort_order")
	public int getSortOrder()
	{
		return sortOrder;
	}

	public void setLocation(double[] locations)
	{
		this.location = locations;
	}
}