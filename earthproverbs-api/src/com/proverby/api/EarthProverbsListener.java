package com.proverby.api;

public interface EarthProverbsListener
{
	/**
	 * Let the caller know that the results are still loading
	 */
	public void loading();

	/**
	 * Let the caller know that the loading process is complete. We can let the
	 * then call the result set from the api.
	 */
	public void finished(ParseStatus status);
}