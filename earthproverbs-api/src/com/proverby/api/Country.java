package com.proverby.api;

import java.util.List;

public interface Country
{
	/** 
	 * Get the ISO two letter country code
	 * @return
	 */
	public String getCode();

	/**
	 * Get the name of the country
	 * @return
	 */
	public String getName();

	/**
	 * Get the location of the country as an array of doubles
	 * @return
	 */
	public double[] getLocation();

	/**
	 * Get the date this record has been last updated
	 * @return
	 */
	public long getLastUpdated();

	/**
	 * Get a list of the all the regions this country belongs to
	 * @return
	 */
	public List<String> getRegions();

	/**
	 * Get the url of the country
	 * @return
	 */
	public String getBackground();

	/**
	 * Get the sort order of the country
	 * @return
	 */
	public int getSortOrder();
}