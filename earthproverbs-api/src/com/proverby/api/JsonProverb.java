package com.proverby.api;

import java.util.Collection;
import java.util.HashSet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

class JsonProverb implements Proverb
{
	private String tiny;
	private String description;
	private String peopleGroup;
	private String author;
	private String reference;
	private Integer year;
	private String country;
	private String region;
	
	private Collection<String> tags = new HashSet<String>();

	public JsonProverb()
	{
		
	}
	
	public static JsonProverb create( Proverb source )
	{
		if ( source == null )
			return null;
		JsonProverb json = new JsonProverb();
		json.author = source.getAuthor();
		json.country = source.getCountry();
		json.description = source.getDescription();
		json.tiny = source.getTiny();
		json.peopleGroup = source.getPeopleGroup();
		json.reference = source.getReference();
		json.region = source.getRegion();
		json.tags = source.getTags();
		json.year = source.getYear();
		return json;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	@JsonProperty("people_group")
	public String getPeopleGroup()
	{
		return peopleGroup;
	}

	@JsonProperty("people_group")
	public void setPeopleGroup(String peopleGroup)
	{
		this.peopleGroup = peopleGroup;
	}

	public String getAuthor()
	{
		return author;
	}

	public void setAuthor(String author)
	{
		this.author = author;
	}

	public Integer getYear()
	{
		return year;
	}

	public void setYear(Integer year)
	{
		this.year = year;
	}

	public Collection<String> getTags()
	{
		return tags;
	}

	public void setTags(Collection<String> tags)
	{
		this.tags = tags;
	}

	public String getReference()
	{
		return reference;
	}

	public void setReference(String reference)
	{
		this.reference = reference;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String countryName)
	{
		this.country = countryName;
	}

	public void setRegion(String region)
	{
		this.region = region;
	}

	public String getRegion()
	{
		return region;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tiny == null) ? 0 : tiny.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JsonProverb other = (JsonProverb) obj;
		if (tiny == null)
		{
			if (other.tiny != null)
				return false;
		}
		else if (!tiny.equals(other.tiny))
			return false;
		return true;
	}

	public void setTiny(String tiny)
	{
		this.tiny = tiny;
	}

	@JsonIgnore
	public long getLastUpdated()
	{
		return 0;
	}

	@Override
	public String getTiny()
	{
		return tiny;
	}
}