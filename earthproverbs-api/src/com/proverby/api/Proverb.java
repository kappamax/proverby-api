package com.proverby.api;


import java.util.Collection;

public interface Proverb
{
	/**
	 * Get the proverb description
	 * @return
	 */
	public String getDescription();

	/**
	 * Get the people group the proverb is associated with
	 * @return
	 */
	public String getPeopleGroup();

	/** 
	 * Get the author of the proverb if available
	 * @return
	 */
	public String getAuthor();

	/**
	 * Get the reference of the proverb if found in a book
	 * @return
	 */
	public String getReference();

	/**
	 * Get the year the proverb was first recorded
	 * @return
	 */
	public Integer getYear();

	/**
	 * Get the country this proverb is associated with
	 * @return
	 */
	public String getCountry();

	/**
	 * Get the region associated with this proverb
	 * @return
	 */
	public String getRegion();

	/**
	 * Get the date in milliseconds that this record was last updated
	 * @return
	 */
	public long getLastUpdated();

	/**
	 * Get the tiny url key for this proverb
	 * @return
	 */
	public String getTiny();

	/**
	 * Get a collection of tags used by this proverb
	 * @return
	 */
	public Collection<String> getTags();
}