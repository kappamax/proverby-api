package com.proverby.api;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import com.proverby.api.param.Parameter;

public class Query
{
	Set<Parameter> params;
	QueryUrl url;

	private Query(QueryUrl url)
	{
		this.params = new HashSet<Parameter>();
		this.url = url;
	}

	private static Query create(QueryUrl url)
	{
		if (url == null) return null;
		return new Query(url);
	}

	public static Query createProverbListQuery()
	{
		return create(QueryUrl.LIST_PROVERBS);
	}

	public static Query createCountryListQuery()
	{
		return create(QueryUrl.LIST_COUNTRIES);
	}

	static Query createTagListQuery()
	{
		return create(QueryUrl.LIST_TAGS);
	}

	static Query createRegionListQuery()
	{
		return create(QueryUrl.LIST_REGIONS);
	}

	public static Query createSearchProverbQuery()
	{
		return create(QueryUrl.SEARCH_PROVERBS);
	}

	public static Query createListProverbByTagQuery()
	{
		return create(QueryUrl.LIST_TAG_PROVERBS);
	}

	public static Query createListProverbByRegionQuery()
	{
		return create(QueryUrl.LIST_REGION_PROVERBS);
	}

	public static Query createListProverbByCountryQuery()
	{
		return create(QueryUrl.LIST_COUNTRY_PROVERBS);
	}

	public static Query createDailyProverbQuery()
	{
		return create(QueryUrl.DAILY_PROVERB);
	}

	public Query addParameter(Parameter... params)
	{
		if (params != null)
		{
			for (Parameter param : params)
			{
				this.params.add(param);
			}
		}
		return this;
	}

	URL getURL() throws MalformedURLException
	{
		StringBuilder urlString = new StringBuilder(url.getUrl());
		for (Parameter param : params)
		{
			urlString.append(param.toString());
		}
		return new URL(urlString.toString());
	}
}